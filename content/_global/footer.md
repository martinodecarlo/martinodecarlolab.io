+++
fragment = "footer"
#disabled = true
date = "2020-10-10"
weight = 1200
#background = ""

menu_title = "Spink"

[asset]
  title = "Spink"
  image = "spink.png"
  text = "Speak by blink"
  url = "#"
+++

#### Spink

Spink:
Android app to speak using eye blinking
Provided by Martino De Carlo
