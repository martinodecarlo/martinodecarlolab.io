+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Us"
#subtitle = ""
+++

<br>
Spink is a new project for letting people speak using the blinking of their eyes.

It has been initially thought for people suffering from neurodegenerative deseases. Expensive hardware that let people speak with their eyes is usually out of budget for many people. Spink is meant to subsitute that costly hardware with a single app.


---
**Something about me**

My name is Martino De Carlo. I am an Electronic Engineer and, currently, a research fellow at the Politecnico di Bari.

For any information or doubt you can contact me at info@spink.app.

