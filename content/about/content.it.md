+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "Chi siamo"
#subtitle = ""
+++

<br>
Spink è un nuovo progetto, nato con l'obiettivo di permettere di parlare con il battito degli occhi.

L'idea è nata per aiutare le persone che soffrono di malattie neurodegenerative. 
Gli hardware costosi che permettono alle persone di parlare tracciando gli occhi sono solitamente troppo costosi. Spink è concepito per sostiturie quell'hardware con una singola app.

---
**Qualcosa su di me**

Sono Martino De Carlo, un ingegnere elettronico e, attualmente, ricercatore presso il Politecnico di Bari. 

Per qualsiasi informazione o dubbio puoi contattarmi all'indirizzo info@spink.app.

