+++
title = "Worldwide chat"
weight = 30

[asset]
  icon = "fas fa-globe-americas"
+++

You can chat with other app users around the world, with a global chat.
