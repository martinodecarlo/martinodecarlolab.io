+++
title = "Just with your eyes"
weight = 100

[asset]
  icon = "fas fa-eye"
+++

You can control all the app, only by blinking your eyes
