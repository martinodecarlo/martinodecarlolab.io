+++
title = "Parla con il battito degli occhi"
weight = 10

[asset]
  icon= "far fa-comments"
  
  # url="#"
+++

Dai voce ai tuoi pensieri con il battito dei tuoi occhi. Puoi scrivere parola per parola e riprodurre quello che scrivi.