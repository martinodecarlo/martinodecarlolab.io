+++
title = "Servizio di chat"
weight = 30

[asset]
  icon = "fas fa-globe-americas"
+++

Puoi chattare con altri utenti nel mondo, attraverso una chat.