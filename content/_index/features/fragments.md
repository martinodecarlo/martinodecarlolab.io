+++
title = "Speak by blink"
weight = 10

[asset]
  icon= "far fa-comments"
  
  # url="#"
+++

Give voice to your thoughts by blinking your eyes. You can write each word and reproduce what you write.