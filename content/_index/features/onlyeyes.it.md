+++
title = "Solo con i tuoi occhi"
weight = 100

[asset]
  icon = "fas fa-eye"
+++

Controlla tutta l'app con il solo battito degli occhi