+++
title = "Minigames"
weight = 100

[asset]
  icon = "fas fa-gamepad"
+++

The presence of minigames enables new challenges just with your eyes
