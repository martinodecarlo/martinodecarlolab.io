+++
title = "Minigiochi"
weight = 100

[asset]
  icon = "fas fa-gamepad"
+++

Con i minigiochi potrai cimentarti in nuove sfide solo con il battito degli occhi