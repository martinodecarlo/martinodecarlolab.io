+++
title = "Testo predittivo"
weight = 20

[asset]
  icon = "fas fa-keyboard"
+++

La predizione del testo comprende quello che vuoi dire, rendendo il processo di scrittura molto più veloce.