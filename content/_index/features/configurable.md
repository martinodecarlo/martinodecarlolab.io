+++
title = "Predictive text"
weight = 20

[asset]
  icon = "fas fa-keyboard"
+++

Predictive text understands what you want to say, making the writing process much faster.
