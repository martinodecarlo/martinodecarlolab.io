+++
fragment = "contact"
#disabled = true
date = "2017-09-10"
weight = 1100
#background = "light"
form_name = "defaultContact"

title = "Contattaci"
subtitle  = "*Chiedi informazioni sull'app o qualsiasi dubbio*"

# PostURL can be used with backends such as mailout from caddy
post_url = "https://formspree.io/f/xeqpeykw" #default: formspree.io
email = "martinodecarlo@gmail.com"
button = "Send Button" # defaults to theme default
button_text = "Invia messaggio"
#netlify = false

# Optional google captcha
#[recaptcha]
#  sitekey = ""

[message]
  #success = "" # defaults to theme default
  #error = "" # defaults to theme default

# Only defined fields are shown in contact form
[fields.name]
  text = "Il tuo nome *"
  #error = "" # defaults to theme default

[fields.email]
  text = "La tua email *"
  #error = "" # defaults to theme default

[fields.phone]
  text = "Numero di telefono *"
  #error = "" # defaults to theme default

[fields.message]
  text = "Testo del messaggio *"
  #error = "" # defaults to theme default

# Optional hidden form fields
# Fields "page" and "site" will be autofilled
[[fields.hidden]]
  name = "page"

[[fields.hidden]]
  name = "someID"
  value = "example.com"
+++
