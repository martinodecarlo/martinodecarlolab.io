+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "light" # can influence the text color
particles = true

title = "Spink - Speak by blink"
subtitle = "Speak by blink"
 tertiary =  "#68847E"
 
[header]
  image = "header.jpg"

[asset]
  image = "spink.png"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Features"
  url = "#features"
  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

  [[buttons]]
  text = "Demonstration"
  url = "#video"
  color = "primary"

[[buttons]]
  text = "Download"
  url = "https://play.google.com/store/apps/details?id=com.martinodecarlo.spink"
  color = "success"



[[buttons]]
  text = "Contact"
  url = "#contact"
  color = "warning"
+++
