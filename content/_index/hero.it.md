+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "light" # can influence the text color
particles = true

title = "Spink - Parla con il battito degli occhi"
subtitle = "Speak by blink"
 tertiary =  "#68847E"
 
[header]
  image = "header.jpg"

[asset]
  image = "spink.png"
  width = "500px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Funzionalità"
  url = "#features-index"
  color = "info" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary

  [[buttons]]
  text = "Dimostrazione"
  url = "#video.it"
  color = "primary"

[[buttons]]
  text = "Scarica ora"
  url = "https://play.google.com/store/apps/details?id=com.martinodecarlo.spink"
  color = "success"



[[buttons]]
  text = "Contattaci"
  url = "#contact"
  color = "warning"
+++
